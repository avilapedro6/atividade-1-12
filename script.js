let changeIcon = function(icon) {
    icon.classList.toggle('fa-times')
}
function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  function mostrarNFilhos() {
    if(document.getElementById('casado').checked) {
        document.getElementById('nFilhosL').style.display='block';
        document.getElementById('nFilhosI').style.display='block';
    } else{
        document.getElementById('nFilhosL').style.display='none';
        document.getElementById('nFilhosI').style.display='none';
        }
  }

  function mostrarNomePlSaude() {
    if(document.getElementById('plSaude').checked) {
        document.getElementById('plSaudeNomeL').style.display='block';
        document.getElementById('plSaudeNomeI').style.display='block';
    } else{
        document.getElementById('plSaudeNomeL').style.display='none';
        document.getElementById('plSaudeNomeI').style.display='none';
        }
  }